package starter.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;
import starter.status.SearchPostCodeStatus;

public class SearchPostcodeStepDefinitions {

    @Steps
    SearchPostCodeStatus searchPostCodeStatus;

    @Given("I search for the postcode \"(.*)\"")
    public void i_send_the_request(String postCode) {
        searchPostCodeStatus.searchPostCode(postCode);
    }

    @Then("search is executed successfully")
    public void search_is_executed( ) {
        searchPostCodeStatus.searchIsExecutedSuccessfully();
    }

}
