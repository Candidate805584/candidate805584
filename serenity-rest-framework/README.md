##Get the code

Git:
git clone https://gitlab.com/Candidate805584/candidate805584.git

## Serenity Rest project
This project is to test the postcodes API and check the status code..

### The project directory structure
The project has build scripts for Maven and with below directory structure:
```Gherkin
src
  + test
    + java                                Test runner and supporting code
      +++ CucumberTestSuite                 To execute the tests
    + resources
      + features                          
        +status                            Feature files (search_postcode.feature)

```

### Dependencies used
Below are few of the included dependencies 
serenity-cucumber4
serenity-rest-assured
hamcrest-all

### Executing the tests
CucumberTestSuite test runner class can be used to run the project or by 'mvn clean verify' command.

### Results
Results are recorded in target/site/serenity directory.

