package net.Serinity.ho.navigation;

import net.thucydides.core.annotations.Step;

public class NavigateTo {

    HomeOfficeHomePage homeOfficeHomePage;

    @Step("Open the home office site")
    public void theHomeOfficeSitePage() {
        homeOfficeHomePage.open();
    }
}
